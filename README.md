# Tkinter Canvas Editor

A Editor for Python Tkinter Canvas, wich is a powerful tool that __allows users to create and manipulate graphical objects and designs within a GUI application__. The editor enables users to create *geometric graphics*, *draw shapes*, *add text* and *images*, and apply *various styles* and *attributes* to their designs.

## Canvas guide

![View the quick Canvas guide in the .PDF format](doc/canvas.pdf)
