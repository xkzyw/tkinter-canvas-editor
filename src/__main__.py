import tkinter

import toolBar
import configBar
import pprint


class App(tkinter.Tk):
	def __init__(self):
		super().__init__()

		self.geometry("1920x1080")
		self.resizable(False, False)
		self.title("Canvas editor")
		self.canvas_height = 900
		self.canvas_width = 1600
		self.opt = ""
		self.h = True
		self.co_list = list()
		self.finished = False

		self.click_coo = None

		self.main_frame = tkinter.Frame(self)
		self.main_frame.grid()

		self.main_frame.kv = dict()
		self.main_frame.kv["selected_items_rect"] = list()
		self.main_frame.kv["selected_items"] = tuple()
		self.main_frame.kv["multiselected_items_rect"] = list()
		self.main_frame.kv["tmp_items"] = list()

		self.canvas = tkinter.Canvas(self.main_frame, height=self.canvas_height, width=self.canvas_width, relief=tkinter.RAISED, borderwidth=2)
		self.canvas.grid(column=1, row=1)

		self.canvas.photos = {}

		self.canvas.bind("<Button-1>", self.singleClick)
		self.canvas.bind("<Control-a>", self.selectAll)
		self.canvas.bind("<Control-d>", self.delete_item)
		self.canvas.bind("<B1-Motion>", self.motion)
		self.canvas.bind("<ButtonRelease-1>", self.button1Released)

		self.main_frame.cb = configBar.ConfigBar(self.main_frame, self.canvas)
		self.main_frame.tb = toolBar.ToolBar(self.main_frame, self.canvas)

	def button1Released(self, e):
		self.multireset(e)
		self.finished = True
		if self.opt == "draw":
			self.canvas.focus_set()
			for i in self.main_frame.kv["tmp_items"]:
				self.canvas.delete(i)
				self.canvas.focus_set()
			self.main_frame.cb.conf.draw(self.co_list)

	def motion(self, e):
		if self.opt == "move":
			self.move_items(e)
			self.reset(e)
			self.showSelected()
		elif self.opt == "draw":
			self.co_list.append(e.x)
			self.co_list.append(e.y)

			if not self.finished:
				self.canvas.focus_set()
				for i in self.main_frame.kv["tmp_items"]:
					self.canvas.delete(i)
					self.canvas.focus_set()
			self.main_frame.kv["tmp_items"].clear()

			r = self.main_frame.cb.conf.draw(self.co_list)
			self.main_frame.kv["tmp_items"].append(r)

		else:
			x1, y1 = self.click_coo
			x2, y2 = e.x, e.y

			if (x1 - x2 < 0 and y1 - y2 < 0) or (x1 - x2 < 0 and y1 - y2 > 0):
				ol = "blue"
			else:
				ol = "green"

			self.reset(e)
			r = self.canvas.create_rectangle(x1, y1, x2, y2, width=5, outline=ol)
			self.main_frame.kv["multiselected_items_rect"].append(r)

			if (x1 - x2 < 0 and y1 - y2 < 0) or (x1 - x2 < 0 and y1 - y2 > 0):
				self.incSelect(r)
			else:
				self.overSelect(r)

			if len(self.main_frame.kv["selected_items"]) == 1:
				if not self.h:
					self.main_frame.cb.changeConfig(self.main_frame.kv["selected_items"][0])
				self.h = True
			else:
				self.h = False

	def move_items(self, e):
		for i in self.main_frame.kv["selected_items"]:
			self.canvas.moveto(i, e.x, e.y)

	def incSelect(self, r):
		self.main_frame.kv["selected_items"] = tuple(filter(lambda x: x != r, list(self.canvas.find_enclosed(*self.canvas.coords(r)))))
		self.showSelected()

	def overSelect(self, r):
		self.main_frame.kv["selected_items"] = tuple(filter(lambda x: x != r, list(self.canvas.find_overlapping(*self.canvas.coords(r)))))
		self.showSelected()

	def singleClick(self, e: tkinter.Event):
		self.click_coo = e.x, e.y
		self.co_list.clear()
		self.finished = False
		self.co_list.extend(self.click_coo)
		self.main_frame.kv["selected_items"] = ()
		if self.opt != "draw":
			self.reset(e)
			self.selectOne(e)
			if len(self.main_frame.kv["selected_items"]) == 1:
				self.main_frame.cb.changeConfig(self.main_frame.kv["selected_items"][0])
			else:
				self.main_frame.cb.changeConfig(None)

	def showSelected(self):
		pad = 10

		i = self.main_frame.kv["selected_items"]
		if isinstance(i, tuple):
			for t in i:
				x1, y1, x2, y2 = self.canvas.bbox(t)

				r = self.canvas.create_rectangle(x1-pad, y1-pad, x2+pad, y2+pad, width=5)
				self.main_frame.kv["selected_items_rect"].append(r)
		else:
			x1, y1, x2, y2 = self.canvas.bbox(i)

			r = self.canvas.create_rectangle(x1-pad, y1-pad, x2+pad, y2+pad, width=5)
			self.main_frame.kv["selected_items_rect"].append(r)

	def select_tags(self, *tags):
		tmp = []
		for tag in tags:
			tmp.extend(self.canvas.find_withtag(tag))

		self.main_frame.kv["selected_items"] = tuple(tmp)
		self.reset(None)
		self.showSelected()

	def delete_item(self, e):
		self.reset(e)
		for i in self.main_frame.kv["selected_items"]:
			self.canvas.delete(i)

	def reset(self, e):
		self.canvas.focus_set()

		self.multireset(e)
		for i in self.main_frame.kv["selected_items_rect"]:
			self.canvas.delete(i)
		self.main_frame.kv["selected_items_rect"].clear()

	def multireset(self, e):
		for i in self.main_frame.kv["multiselected_items_rect"]:
			self.canvas.delete(i)
		self.main_frame.kv["multiselected_items_rect"].clear()

	def selectAll(self, e):
		if len(self.main_frame.kv["selected_items"]) == 0:
			self.main_frame.kv["selected_items"] = self.canvas.find_all()
			self.showSelected()

	def selectOne(self, e: tkinter.Event):
		pad = 5
		x1, y1, x2, y2 = e.x-pad, e.y-pad, e.x+pad, e.y+pad
		if self.canvas.find_overlapping(x1, y1, x2, y2):
			self.main_frame.kv["selected_items"] = (self.canvas.find_overlapping(x1, y1, x2, y2)[0],)
			self.showSelected()

if __name__ == "__main__":
	App().mainloop()
