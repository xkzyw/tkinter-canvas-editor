import tkinter

class Select_tl(tkinter.Toplevel):
	def __init__(self, master):
		super().__init__(master)

		self.geometry("400x200+800+200")
		self.tags = tkinter.StringVar()

		self.create_widget()

	def create_widget(self):
		main_frame = tkinter.Frame(self)
		main_frame.grid()

		lbl = tkinter.Label(self, text="Seleziona per tag")
		lbl.grid(row=2, column=0, sticky=tkinter.W)

		e = tkinter.Entry(self, width=30, textvariable=self.tags)
		e.grid(row=3, column=0, columnspan=10)

		btn = tkinter.Button(self, text="OK", command=self.ok)
		btn.grid(row=4, column=0, columnspan=10, sticky=tkinter.E, pady=30)

	def ok(self):
		tags = list(filter(lambda x: len(x.strip()) != 0, self.tags.get().split(",")))
		self.master.master.master.select_tags(tags)

		self.destroy()