import tkinter
import utilities
import tkinter.filedialog

import tkinter.messagebox as tmb


class Bitmap_config(tkinter.Frame):
	def __init__(self, master, c: tkinter.Canvas, itemId=None):
		super().__init__(master)

		self.id = itemId

		self.c = c
		self.x1 = tkinter.DoubleVar(value=0)
		self.y1 = tkinter.DoubleVar(value=0)

		self.anchor = tkinter.StringVar(value="center")
		self.background = tkinter.StringVar(value="white")
		self.bitmap = ""
		self.foreground = tkinter.StringVar(value="black")
		self.state = tkinter.StringVar(value="normal")
		self.filename = tkinter.StringVar()
		self.tags = tkinter.StringVar()

		self.stxt = tkinter.StringVar(value="Crea")

		if not self.id is None:
			self.stxt.set("Aggiorna")
			self.resume()

		self.grid(padx=10)
		self.create_widget()

	def draw(self, l):
		if len(l) == 2:
			return

		x1, y1 = l[-2], l[-1]

		self.x1.set(x1)
		self.y1.set(y1)

		return self.submit()

	def resume(self):
		x1, y1 = self.c.coords(self.id)

		self.x1.set(x1)
		self.y1.set(y1)

		self.bitmap = self.c.itemcget(self.id, "bitmap")
		self.filename.set(self.bitmap.split("/")[-1])
		self.anchor.set(self.c.itemcget(self.id, "anchor"))
		self.background.set(self.c.itemcget(self.id, "background"))
		self.foreground.set(self.c.itemcget(self.id, "foreground"))
		self.state.set(self.c.itemcget(self.id, "state"))
		self.tags.set(",".join(self.c.itemcget(self.id, "tags").split(" ")))

	def create_widget(self):
		lbl = tkinter.Label(self, text="Punto di referimento: ")
		lbl.grid(column=0, row=0, columnspan=10, sticky=tkinter.W)

		xlbl = tkinter.Label(self, text="x: ")
		xlbl.grid(column=0, row=2)

		e1 = tkinter.Entry(self, textvariable=self.x1, width=7)
		e1.grid(column=1, row=2)

		ylbl = tkinter.Label(self, text="y: ")
		ylbl.grid(column=2, row=2)

		e2 = tkinter.Entry(self, textvariable=self.y1, width=7)
		e2.grid(column=3, row=2)

		lbl = tkinter.Label(self, text="Path del bitmap: ")
		lbl.grid(column=0, row=3, columnspan=10, sticky=tkinter.W, pady=(10, 0))

		file_btn = tkinter.Button(self, text="Apri un file", command=self.selectFile)
		file_btn.grid(column=0, row=4, columnspan=5)

		lbl = tkinter.Label(self, textvariable=self.filename)
		lbl.grid(column=6, row=4, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="anchor:")
		lbl1.grid(column=0, row=5, columnspan=10, pady=(30, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.anchor, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["center", "nw", "n", "ne", "e", "se", "s", "sw", "w"]:
			m.add_radiobutton(label=s, variable=self.anchor)

		m_button.grid(column=0, row=6, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="background:")
		lbl1.grid(column=0, row=7, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.background, width=10)
		e5.grid(column=0, row=8, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="foreground:")
		lbl1.grid(column=0, row=9, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.foreground, width=10)
		e5.grid(column=0, row=10, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="state:")
		lbl1.grid(column=0, row=11, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.state, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		for s in ["normal", "disabled", "hidden"]:
			m.add_radiobutton(label=s, variable=self.state)

		m_button["menu"] = m

		m_button.grid(column=0, row=12, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl = tkinter.Label(self, text="tags")
		lbl.grid(column=0, row=13, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.tags, width=30)
		e5.grid(column=0, row=14, columnspan=10, sticky=tkinter.W)

		sub_btn = tkinter.Button(self, command=self.submit, textvariable=self.stxt)
		sub_btn.grid(column=0, row=15, pady=40, columnspan=5)

	def selectFile(self):
		name = tkinter.filedialog.askopenfilename(filetypes=[("X bitmap Image", "*.xbm")])
		if not name is None:
			self.bitmap = name
			self.filename.set(name.split("/")[-1])

	def submit(self):
		res = utilities.are_number(self.x1, self.y1)

		if not res:
			tmb.showerror("Errore", "Alcuni parametri richiedono dei numeri interi.")
			return

		tags = [x.strip() for x in list(filter(lambda x: len(x.strip()) != 0, self.tags.get().split(",")))]

		try:
			if self.id is None:
				return self.c.create_bitmap(self.x1.get(), self.y1.get(), bitmap="@"+self.bitmap, background=self.background.get(), foreground=self.foreground.get(), state=self.state.get(), anchor=self.anchor.get(), tags=tags)
			else:
				self.c.coords(self.id, self.x1.get(), self.y1.get())
				self.c.itemconfigure(self.id, bitmap=self.bitmap, background=self.background.get(), foreground=self.foreground.get(), state=self.state.get(), anchor=self.anchor.get(), tags=self.tags.get())
				self.master.master.master.reset([])
				self.master.master.master.showSelected()
		except Exception as e:
			tmb.showerror("Errore", str(e))
			return
