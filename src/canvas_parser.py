import tkinter
import functools
import json
import tkinter.messagebox as tmb

class Parser:
	def __init__(self, canvas: tkinter.Canvas = None, filename: str = ""):
		self.__canvas: tkinter.Canvas | None = None
		self.__filename: str | None = None

		self.canvas = canvas
		self.filename = filename

		# Export of window item is not supported
		self.map = {"line": functools.partial(self.get_code,
		                                      ["arrow", "arrowshape", "fill", "smooth", "state", "tags", "width"],
		                                      "line"),
		            "oval": functools.partial(self.get_code, ["fill", "outline", "state", "tags", "width"], "oval"),
		            "polygon": functools.partial(self.get_code, ["fill", "outline", "smooth", "state", "tags", "width"],
		                                         "polygon"),
		            "rectangle": functools.partial(self.get_code, ["fill", "outline", "state", "tags", "width"],
		                                           "rectangle"),
		            "text": functools.partial(self.get_code,
		                                      ["anchor", "angle", "fill", "font", "state", "tags", "underline", "width",
		                                       "text"], "text"),
		            "arc": functools.partial(self.get_code,
		                                    	["start", "extent", "style", "outline", "width", "fill", "state", "tags"],
		                                     "arc"),
		            "bitmap": functools.partial(self.get_code,
		                                        ["anchor", "background", "bitmap", "foreground", "state", "tags"],
		                                        "bitmap"),
		            "image": functools.partial(self.get_code, ["anchor", "image", "state", "tags"], "image")}
		
		self.create_map = {
			"line": functools.partial(self.canvas.create_line),
			"arc": functools.partial(self.canvas.create_arc),
			"oval": functools.partial(self.canvas.create_oval),
			"text": functools.partial(self.canvas.create_text),
			"rectangle": functools.partial(self.canvas.create_rectangle),
			"polygon": functools.partial(self.canvas.create_polygon),
			"bitmap": functools.partial(self.canvas.create_bitmap),
			"image": functools.partial(self.canvas.create_image)
		}

	@property
	def canvas(self):
		return self.__canvas

	@canvas.setter
	def canvas(self, val: tkinter.Canvas):
		if not isinstance(val, tkinter.Canvas):
			raise TypeError(f"Excepted tkinter.Canvas, found {type(val)}")

		self.__canvas = val

	@property
	def filename(self):
		return self.__filename

	@filename.setter
	def filename(self, val: str):
		if not isinstance(val, str):
			raise TypeError(f"Excepted str, found {type(val)}")

		self.__filename = val

	def to_json(self):
		res = []

		for item in self.canvas.find_all():
			res.append(self.map[self.canvas.type(item)](item, "json"))

		with open(self.filename, "w") as out:
			json.dump(res, out, indent="\t", separators=(",", ": "))

	def from_json(self):
		with open(self.filename, "r") as input_file:
			data = json.load(input_file)

		for item in data:
			try:
				c = item["configs"]
				if item["type"] == "image":
					p = tkinter.PhotoImage(master=self.canvas, file=item["image"])
					c["image"] = p
				id = self.create_map[item["type"]](*item["coordinates"], **item["configs"])

				if item["type"] == "image":
					self.canvas.photos[id] = {"img_obj": p, "path": item["image"]}
			except Exception as e:
				tmb.showerror("Errore", "Errore nel file json, importazione parziale o nullo")
				print(e)
				return
			

	def to_py(self):
		code = """\
import tkinter


class canvas_app(tkinter.Tk):
	def __init__(self):
		super().__init__()
		self.geometry("1920x1080")
		self.resizable(False, False)
		self.title("Canvas app")
		
		self.create_widget()
		
	def create_widget(self):
		main_frame = tkinter.Frame(self)
		main_frame.pack(expand=True, fill=tkinter.BOTH)
		
		canvas = tkinter.Canvas(main_frame, height=900, width=1600, relief=tkinter.RAISED, borderwidth=2)
		canvas.pack(expand=True, fill=tkinter.BOTH)
"""
		for item in self.canvas.find_all():
			if self.canvas.type(item) == "image":
				code += f"{self.addImage(item)}"
			text = f'\t\t{self.map[self.canvas.type(item)](item)}\n'
			code += text

		code += """\


if __name__ == "__main__":
	canvas_app().mainloop()
"""
		with open(self.filename, "w") as output_file:
			output_file.write(code)

	def addImage(self, id):
		path = self.canvas.photos[id]["path"]
		return f"""\
		self.image{id} = tkinter.PhotoImage(master=self, file="{path}")
"""

	def __get_config(self, itemId, configName):
		if self.canvas.type(itemId) == "image" and configName == "image":
			return f'{configName}=self.image{itemId}'
		return f'{configName}="{self.canvas.itemcget(itemId, configName)}"'

	def __get_json_config(self, itemId, configName, d):
		if self.canvas.type(itemId) == "image" and configName == "image":
			d[configName] = self.canvas.photos[itemId]["path"]
		else:
			d["configs"][configName] = self.canvas.itemcget(itemId, configName)

	def get_code(self, configNames, itemType, itemId, type="py"):
		if type == "py":
			plist = list()
			coo = self.canvas.coords(itemId)
			plist.append(str(coo)[1:-1])
		elif type == "json":
			plist = dict()
			coo = self.canvas.coords(itemId)
			plist["type"] = itemType
			plist["coordinates"] = coo
			plist["configs"] = {}

		for c in configNames:
			if type == "py":
				plist.append(self.__get_config(itemId, c))
			elif type == "json":
				self.__get_json_config(itemId, c, plist)

		if type == "py":
			param = ", ".join(plist)
			return f"canvas.create_{itemType}({param})"
		elif type == "json":
			return plist