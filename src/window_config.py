import copy
import tkinter
import button_tl
import utilities
import tkinter.messagebox as tmb
import radio_tl

w_map = {"button": button_tl.Button_tl,
         "radio": radio_tl.Radio_tl}


class Window_config(tkinter.Frame):
	def __init__(self, master, c: tkinter.Canvas):
		super().__init__(master)

		self.c = c
		self.x1 = tkinter.IntVar(value=0)
		self.y1 = tkinter.IntVar(value=0)

		self.anchor = tkinter.StringVar(value="center")
		self.state = tkinter.StringVar(value="normal")
		self.width = tkinter.IntVar(value=0)
		self.window = None
		self.height = tkinter.IntVar(value=0)
		self.widget = tkinter.StringVar(value="")
		self.tags = tkinter.StringVar()

		self.grid(padx=10)
		self.create_widget()

	def create_widget(self):
		lbl = tkinter.Label(self, text="Punto di referimento: ")
		lbl.grid(column=0, row=0, columnspan=10, sticky=tkinter.W)

		xlbl = tkinter.Label(self, text="x: ")
		xlbl.grid(column=0, row=2)

		e1 = tkinter.Entry(self, textvariable=self.x1, width=7)
		e1.grid(column=1, row=2)

		ylbl = tkinter.Label(self, text="y: ")
		ylbl.grid(column=2, row=2)

		e2 = tkinter.Entry(self, textvariable=self.y1, width=7)
		e2.grid(column=3, row=2)

		lbl1 = tkinter.Label(self, text="anchor:")
		lbl1.grid(column=0, row=3, columnspan=10, pady=(30, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.anchor, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["center", "nw", "n", "ne", "e", "se", "s", "sw", "w"]:
			m.add_radiobutton(label=s, variable=self.anchor)

		m_button.grid(column=0, row=4, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="width:")
		lbl1.grid(column=0, row=5, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.width, width=10)
		e5.grid(column=0, row=6, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="height:")
		lbl1.grid(column=0, row=7, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.height, width=10)
		e5.grid(column=0, row=8, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="state:")
		lbl1.grid(column=0, row=9, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.state, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["normal", "disabled", "hidden"]:
			m.add_radiobutton(label=s, variable=self.state)

		m_button.grid(column=0, row=10, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="widget:")
		lbl1.grid(column=0, row=11, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.widget, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10, )
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["button", "radio"]:
			m.add_radiobutton(label=s, variable=self.widget, command=self.cw)

		m_button.grid(column=0, row=12, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl = tkinter.Label(self, text="tags")
		lbl.grid(column=0, row=13, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.tags, width=30)
		e5.grid(column=0, row=14, columnspan=10, sticky=tkinter.W)

		sub_btn = tkinter.Button(self, command=self.submit, text="Aggiorna")
		sub_btn.grid(column=0, row=15, pady=40, columnspan=10)

	def draw(self, l):
		if len(l) == 2:
			return

		x1, y1 = l[-2], l[-1]

		self.x1.set(x1)
		self.y1.set(y1)

		return self.submit()

	def submit(self):
		res = utilities.are_number(self.x1, self.y1, self.width)
		tags = list(filter(lambda x: len(x.strip()) != 0, self.tags.get().split(",")))

		if not res:
			tmb.showerror("Errore", "Alcuni parametri richiedono dei numeri interi.")
			return

		if self.height.get() < 0:
			tmb.showerror("Errore", "'height' deve essere positivo")
			return

		if self.width.get() < 0:
			tmb.showerror("Errore", "'width' deve essere positivo")
			return

		try:
			return self.c.create_window(self.x1.get(), self.y1.get(), window=self.window,
			                            state=self.state.get(), anchor=self.anchor.get(), width=self.width.get(),
			                            height=self.height.get(), tags=tags)
		except Exception as e:
			tmb.showerror("Errore", str(e))
			return

	def cw(self):
		tl: tkinter.Toplevel = w_map[self.widget.get()](self)
		tl.geometry("400x200+800+200")
