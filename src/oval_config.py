import tkinter
import utilities

import tkinter.messagebox as tmb


class Oval_config(tkinter.Frame):
	def __init__(self, master, c: tkinter.Canvas, itemId=None):
		super().__init__(master)

		self.id = itemId

		self.c = c
		self.x1 = tkinter.DoubleVar(value=0)
		self.x2 = tkinter.DoubleVar(value=0)

		self.y1 = tkinter.DoubleVar(value=0)
		self.y2 = tkinter.DoubleVar(value=0)

		self.width = tkinter.DoubleVar(value=1)
		self.outline = tkinter.StringVar(value="black")
		self.fill = tkinter.StringVar(value="")
		self.state = tkinter.StringVar(value="normal")
		self.tags = tkinter.StringVar()
		self.stxt = tkinter.StringVar(value="Crea")

		if not self.id is None:
			self.stxt.set("Aggiorna")
			self.resume()

		self.grid(padx=10)
		self.create_widget()

	def resume(self):
		x1, y1, x2, y2 = self.c.coords(self.id)

		self.x1.set(x1)
		self.x2.set(x2)
		self.y1.set(y1)
		self.y2.set(y2)

		self.fill.set(self.c.itemcget(self.id, "fill"))
		self.outline.set(self.c.itemcget(self.id, "outline"))
		self.state.set(self.c.itemcget(self.id, "state"))
		self.width.set(self.c.itemcget(self.id, "width"))
		self.tags.set(",".join(self.c.itemcget(self.id, "tags").split(" ")))

	def draw(self, l):
		if len(l) == 2:
			return

		x1, y1 = l[0], l[1]
		x2, y2 = l[-2], l[-1]

		self.x1.set(x1)
		self.x2.set(x2)
		self.y1.set(y1)
		self.y2.set(y2)

		return self.submit()

	def create_widget(self):
		lbl = tkinter.Label(self, text="Punto alto-sinistra: ")
		lbl.grid(column=0, row=0, columnspan=10, sticky=tkinter.W)

		xlbl = tkinter.Label(self, text="x: ")
		xlbl.grid(column=0, row=2)

		e1 = tkinter.Entry(self, textvariable=self.x1, width=7)
		e1.grid(column=1, row=2)

		ylbl = tkinter.Label(self, text="y: ")
		ylbl.grid(column=2, row=2)

		e2 = tkinter.Entry(self, textvariable=self.y1, width=7)
		e2.grid(column=3, row=2)

		lbl1 = tkinter.Label(self, text="Punto basso-destra: ")
		lbl1.grid(column=0, row=3, columnspan=10, pady=10, sticky=tkinter.W)

		xlbl1 = tkinter.Label(self, text="x: ")
		xlbl1.grid(column=0, row=4)

		e3 = tkinter.Entry(self, textvariable=self.x2, width=7)
		e3.grid(column=1, row=4)

		ylbl1 = tkinter.Label(self, text="y: ")
		ylbl1.grid(column=2, row=4)

		e4 = tkinter.Entry(self, textvariable=self.y2, width=7)
		e4.grid(column=3, row=4)

		lbl1 = tkinter.Label(self, text="width:")
		lbl1.grid(column=0, row=11, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.width, width=10)
		e5.grid(column=0, row=12, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="outline:")
		lbl1.grid(column=0, row=13, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.outline, width=10)
		e5.grid(column=0, row=14, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="fill:")
		lbl1.grid(column=0, row=15, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.fill, width=10)
		e5.grid(column=0, row=16, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="state:")
		lbl1.grid(column=0, row=17, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.state, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["normal", "disabled", "hidden"]:
			m.add_radiobutton(label=s, variable=self.state)

		m_button.grid(column=0, row=18, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl = tkinter.Label(self, text="tags")
		lbl.grid(column=0, row=19, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.tags, width=30)
		e5.grid(column=0, row=20, columnspan=10, sticky=tkinter.W)

		sub_btn = tkinter.Button(self, command=self.submit, textvariable=self.stxt)
		sub_btn.grid(column=0, row=21, pady=40, columnspan=10)

	def submit(self):
		res = utilities.are_number(self.x1, self.x2, self.y1, self.y2, self.width)
		tags = [x.strip() for x in list(filter(lambda x: len(x.strip()) != 0, self.tags.get().split(",")))]

		if not res:
			tmb.showerror("Errore", "Alcuni parametri richiedono dei numeri interi.")
			return

		if self.width.get() <= 0:
			tmb.showerror("Errore", "'width' deve essere posivito")
			return

		try:
			if self.id is None:
				return self.c.create_oval(self.x1.get(), self.y1.get(), self.x2.get(), self.y2.get(), width=self.width.get(), tags=tags, outline=self.outline.get(), fill=self.fill.get(), state=self.state.get())
			else:
				self.c.coords(self.id, self.x1.get(), self.y1.get(), self.x2.get(), self.y2.get())
				self.c.itemconfigure(self.id, width=self.width.get(), tags=tags, outline=self.outline.get(), fill=self.fill.get(), state=self.state.get())
				self.master.master.master.reset([])
				self.master.master.master.showSelected()
		except Exception as e:
			tmb.showerror("Errore", str(e))
			return