import tkinter
import functools

import select_tl
import canvas_parser
import export_tl
import import_tl

class ToolBar(tkinter.Frame):
	def __init__(self, master, c, **kwargs):
		super().__init__(master, kwargs)

		self.canvas: tkinter.Canvas | None = c
		self.grid(column=0, row=0, columnspan=10, sticky=tkinter.W)
		self.obj_var = tkinter.StringVar()
		self.option_add("*tearOff",  False)
		self.obj = ["Arc", "Bitmap", "Image", "Line", "Oval", "Polygon", "Text", "Window", "Rectangle"]
		self.menu_button_text = tkinter.StringVar(value="Arco")
		self.tags = []

		self.create_widget()

	def create_widget(self):
		menu_button = tkinter.Menubutton(self, textvariable=self.menu_button_text, relief=tkinter.SOLID, height=2, width=10, borderwidth=1)

		menu = tkinter.Menu(menu_button)
		menu_button["menu"] = menu

		for o in self.obj:
			menu.add_radiobutton(label=o, variable=self.obj_var, command=functools.partial(self.changeObj, o))

		menu_button.grid(column=0, row=1, padx=4, pady=4)

		self.changeObj("Arc")

		self.btn = tkinter.Button(self, text="Sposta", command=self.move, relief=tkinter.SOLID, height=2, width=10, borderwidth=1)
		self.btn.grid(column=2, row=1)

		self.btn1 = tkinter.Button(self, text="Disegna", command=self.draw, relief=tkinter.SOLID, height=2, width=10, borderwidth=1)
		self.btn1.grid(column=3, row=1)

		self.btn2 = tkinter.Button(self, text="Esporta", command=self.export, relief=tkinter.SOLID, height=2, width=10, borderwidth=1)
		self.btn2.grid(column=4, row=1)

		self.btn2 = tkinter.Button(self, text="Importa", command=self.imp, relief=tkinter.SOLID, height=2, width=10, borderwidth=1)
		self.btn2.grid(column=5, row=1)

		btn = tkinter.Button(self, text="Ricerca per tag", command=self.rt, relief=tkinter.SOLID, height=2, width=10, borderwidth=1)
		btn.grid(column=6, row=1)

	def imp(self):
		import_tl.Import_tl(self, self.canvas)

	def export(self):
		export_tl.Export_tl(self, self.canvas)

	def draw(self):
		if self.master.master.opt == "":
			self.master.master.opt = "draw"
		elif self.master.master.opt == "draw":
			self.master.master.opt = ""
		self.changeOpt()

	def changeOpt(self):
		if self.master.master.opt == "draw":
			self.btn["bg"] = "#d9d9d9"
			self.btn1["bg"] = "white"
		elif self.master.master.opt == "move":
			self.btn1["bg"] = "#d9d9d9"
			self.btn["bg"] = "white"
		else:
			self.btn1["bg"] = "#d9d9d9"
			self.btn["bg"] = "#d9d9d9"

	def rt(self):
		select_tl.Select_tl(self)

	def changeObj(self, msg):
		self.menu_button_text.set(msg)

		self.master.cb.changeConfig(msg)

	def move(self):
		if self.master.master.opt == "":
			self.master.master.opt = "move"
		elif self.master.master.opt == "move":
			self.master.master.opt = ""
		self.changeOpt()
