import tkinter

import arc_config
import bitmap_config
import img_config
import oval_config
import line_config
import text_config
import window_config
import polygon_config
import rec_config

obj_map = {"Arc": arc_config.Arc_config,
           "Bitmap": bitmap_config.Bitmap_config,
           "Image": img_config.Img_config,
           "Oval": oval_config.Oval_config,
           "Line": line_config.Line_config,
           "Text": text_config.Text_config,
           "Window": window_config.Window_config,
           "Polygon": polygon_config.Polygon_config,
           "Rectangle": rec_config.Rec_config}


class ConfigBar(tkinter.Frame):
	def __init__(self, master, c):
		super().__init__(master)

		self.c: tkinter.Canvas = c
		self.conf = None
		self.grid(row=1, column=0, pady=30)
		self.st = tkinter.StringVar(value="Arc")

		self.configure(height=900, width=300, relief=tkinter.RIDGE, borderwidth=1)

	def changeConfig(self, obj):
		for i in self.winfo_children():
			i.destroy()

		if isinstance(obj, str):
			self.conf = obj_map[obj](self, self.c)
			self.st.set(obj)
		elif obj is None:
			self.conf = obj_map[self.st.get()](self, self.c)
		else:
			self.conf = obj_map[self.c.type(obj).capitalize()](self, self.c, obj)
			self.st.set(self.c.type(obj).capitalize())
		self.conf.grid(column=1, row=1, columnspan=10)
		self.grid_propagate(False)
		self.configure(height=900, width=300, relief=tkinter.RIDGE, borderwidth=1)
