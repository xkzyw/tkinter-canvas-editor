from typing import List


def are_number(*args):
	for i in args:
		try:
			float(i.get())
		except Exception:
			return False

	return True
