import tkinter


class Button_tl(tkinter.Toplevel):
	def __init__(self, master):
		super().__init__(master)

		self.grid()

		self.text = tkinter.StringVar()

		self.create_widget()

	def create_widget(self):
		xlbl = tkinter.Label(self, text="text: ")
		xlbl.grid(column=0, row=0)

		e1 = tkinter.Entry(self, textvariable=self.text, width=10)
		e1.grid(column=1, row=1, columnspan=10)

		btn = tkinter.Button(self, text="OK", command=self.ok)
		btn.grid(column=0, row=2, pady=(20, 0), padx=(30, 0), columnspan=10)

	def ok(self):
		self.master.window = tkinter.Button(None, text=self.text.get())
		self.destroy()
