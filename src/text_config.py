import tkinter
import utilities
import tkinter.messagebox as tmb


class Text_config(tkinter.Frame):
	def __init__(self, master, c: tkinter.Canvas, itemId=None):
		super().__init__(master)

		self.id = itemId

		self.c = c
		self.x1 = tkinter.DoubleVar(value=0)
		self.y1 = tkinter.DoubleVar(value=0)

		self.anchor = tkinter.StringVar(value="center")
		self.testo = tkinter.StringVar(value="")
		self.angle = tkinter.DoubleVar(value=0)
		self.fill = tkinter.StringVar(value="")
		self.font = tkinter.StringVar(value="TkDefaultFont")
		self.state = tkinter.StringVar(value="normal")
		self.width = tkinter.DoubleVar(value=10)
		self.underline = tkinter.IntVar(value=-1)
		self.tags = tkinter.StringVar()
		self.stxt = tkinter.StringVar(value="Crea")

		if not self.id is None:
			self.stxt.set("Aggiorna")
			self.resume()

		self.grid(padx=10)
		self.create_widget()

	def resume(self):
		x1, y1 = self.c.coords(self.id)

		self.x1.set(x1)
		self.y1.set(y1)

		self.anchor.set(self.c.itemcget(self.id, "anchor"))
		self.angle.set(self.c.itemcget(self.id, "angle"))
		self.fill.set(self.c.itemcget(self.id, "fill"))
		self.font.set(self.c.itemcget(self.id, "font"))
		self.state.set(self.c.itemcget(self.id, "state"))
		self.underline.set(self.c.itemcget(self.id, "underline"))
		self.width.set(self.c.itemcget(self.id, "width"))
		self.tags.set(",".join(self.c.itemcget(self.id, "tags").split(" ")))
		self.testo.set(self.c.itemcget(self.id, "text"))

	def draw(self, l):
		if len(l) == 2:
			return

		x1, y1 = l[-2], l[-1]

		self.x1.set(x1)
		self.y1.set(y1)

		return self.submit()

	def create_widget(self):
		lbl = tkinter.Label(self, text="Punto di referimento: ")
		lbl.grid(column=0, row=0, columnspan=10, sticky=tkinter.W)

		xlbl = tkinter.Label(self, text="x: ")
		xlbl.grid(column=0, row=2)

		e1 = tkinter.Entry(self, textvariable=self.x1, width=7)
		e1.grid(column=1, row=2)

		ylbl = tkinter.Label(self, text="y: ")
		ylbl.grid(column=2, row=2)

		e2 = tkinter.Entry(self, textvariable=self.y1, width=7)
		e2.grid(column=3, row=2)

		ylbl = tkinter.Label(self, text="Testo: ")
		ylbl.grid(column=0, row=3, columnspan=10, sticky=tkinter.W)

		e2 = tkinter.Entry(self, textvariable=self.testo, width=30)
		e2.grid(column=0, row=4, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="anchor:")
		lbl1.grid(column=0, row=5, columnspan=10, pady=(30, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.anchor, relief=tkinter.SOLID, borderwidth=1, height=1, width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["center", "nw", "n", "ne", "e", "se", "s", "sw", "w"]:
			m.add_radiobutton(label=s, variable=self.anchor)

		m_button.grid(column=0, row=6, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="angle:")
		lbl1.grid(column=0, row=7, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.angle, width=10)
		e5.grid(column=0, row=8, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="fill:")
		lbl1.grid(column=0, row=9, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.fill, width=10)
		e5.grid(column=0, row=10, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="font:")
		lbl1.grid(column=0, row=9, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.font, width=30)
		e5.grid(column=0, row=10, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="state:")
		lbl1.grid(column=0, row=11, columnspan=10, pady=(10, 0), sticky=tkinter.W)


		m_button = tkinter.Menubutton(self, textvariable=self.state, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["normal", "disabled", "hidden"]:
			m.add_radiobutton(label=s, variable=self.state)

		m_button.grid(column=0, row=12, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="width:")
		lbl1.grid(column=0, row=13, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.width, width=10)
		e5.grid(column=0, row=14, columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="underline:")
		lbl1.grid(column=0, row=15, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.underline, width=10)
		e5.grid(column=0, row=16, columnspan=10, sticky=tkinter.W)

		lbl = tkinter.Label(self, text="tags")
		lbl.grid(column=0, row=17, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.tags, width=30)
		e5.grid(column=0, row=18, columnspan=10, sticky=tkinter.W)

		sub_btn = tkinter.Button(self, command=self.submit, textvariable=self.stxt)
		sub_btn.grid(column=0, row=19, pady=40, columnspan=10)

	def submit(self):
		res = utilities.are_number(self.x1, self.y1, self.width, self.angle, self.underline)

		tags = [x.strip() for x in list(filter(lambda x: len(x.strip()) != 0, self.tags.get().split(",")))]

		if not res:
			tmb.showerror("Errore", "Alcuni parametri richiedono dei numeri interi.")
			return

		if self.width.get() <= 0:
			tmb.showerror("Errore", "'width' deve essere positivo")
			return

		try:
			if self.id is None:
				return self.c.create_text(self.x1.get(), self.y1.get(), text=self.testo.get(),
			                    state=self.state.get(), anchor=self.anchor.get(), width=self.width.get(), underline=self.underline.get(), tags=tags, font=self.font.get(), angle=self.angle.get())
			else:
				self.c.coords(self.id, self.x1.get(), self.y1.get())
				self.c.itemconfigure(self.id, text=self.testo.get(), state=self.state.get(), anchor=self.anchor.get(), width=self.width.get(), underline=self.underline.get(), tags=self.tags, font=self.font.get(), angle=self.angle.get())
				self.master.master.master.reset([])
				self.master.master.master.showSelected()
		except Exception as e:
			tmb.showerror("Errore", str(e))
			return
