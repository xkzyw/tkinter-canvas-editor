import tkinter
from typing import Tuple

import utilities
import tkinter.messagebox as tmb

class Line_config(tkinter.Frame):
	def __init__(self, master, c: tkinter.Canvas, itemId = None):
		super().__init__(master)

		self.id = itemId

		self.lb = tkinter.Listbox(self)
		self.c = c
		self.x1 = tkinter.DoubleVar(value=0)
		self.y1 = tkinter.DoubleVar(value=0)

		self.arrow = tkinter.StringVar(value="none")
		self.width = tkinter.DoubleVar(value=1)
		self.smooth = tkinter.IntVar()
		self.fill = tkinter.StringVar(value="black")
		self.state = tkinter.StringVar(value="normal")
		self.tags = tkinter.StringVar()

		self.l1 = tkinter.DoubleVar(value=0)
		self.l2 = tkinter.DoubleVar(value=0)
		self.l3 = tkinter.DoubleVar(value=0)
		self.stxt = tkinter.StringVar(value="Crea")

		if not self.id is None:
			self.stxt.set("Aggiorna")
			self.resume()

		self.grid(padx=10)
		self.create_widget()

	def resume(self):
		s = self.c.coords(self.id)

		for i in range(0, len(s), 2):
			self.lb.insert(tkinter.END, f"({s[i]}, {s[i+1]})")

		self.arrow.set(self.c.itemcget(self.id, "arrow"))
		self.width.set(self.c.itemcget(self.id, "width"))
		if self.c.itemcget(self.id, "smooth") == "true":
			sm = 1
		else:
			sm = 0
		self.smooth.set(sm)
		self.fill.set(self.c.itemcget(self.id, "fill"))
		self.state.set(self.c.itemcget(self.id, "state"))
		self.tags.set(",".join(self.c.itemcget(self.id, "tags").split(" ")))

		l1, l2, l3 = self.c.itemcget(self.id, "arrowshape").split(" ")

		self.l1.set(l1)
		self.l2.set(l2)
		self.l3.set(l3)

	def create_widget(self):
		self.lb.grid(column=0, columnspan=5, rowspan=5, row=0, pady=(20, 0))

		yscrollbar = tkinter.Scrollbar(self, orient=tkinter.VERTICAL, command=self.lb.yview)
		self.lb.configure(yscrollcommand=yscrollbar.set)

		yscrollbar.grid(column=5, rowspan=5, row=0, pady=(20, 0), sticky=tkinter.NS)

		btn = tkinter.Button(self, text="Elimina", command=self.ditem)
		btn.grid(row=2, column=8)

		btn = tkinter.Button(self, text="Modifica", command=self.modify)
		btn.grid(row=3, column=8)

		btn = tkinter.Button(self, text="Inserisci sopra", command=self.add_above)
		btn.grid(row=8, column=0, pady=(10, 30), columnspan=10, sticky=tkinter.W)

		btn = tkinter.Button(self, text="Inserisci sotto", command=self.add_below)
		btn.grid(row=8, column=5, pady=(10, 30), columnspan=10)

		lbl = tkinter.Label(self, text="Inserisci il punto: ")
		lbl.grid(column=0, row=6, columnspan=10, sticky=tkinter.W)

		xlbl = tkinter.Label(self, text="x: ")
		xlbl.grid(column=0, row=7)

		e1 = tkinter.Entry(self, textvariable=self.x1, width=7)
		e1.grid(column=1, row=7)

		ylbl = tkinter.Label(self, text="y: ")
		ylbl.grid(column=2, row=7)

		e2 = tkinter.Entry(self, textvariable=self.y1, width=7)
		e2.grid(column=3, row=7)

		lbl1 = tkinter.Label(self, text="arrow:")
		lbl1.grid(column=0, row=9, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		m_button = tkinter.Menubutton(self, textvariable=self.arrow, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["none", "first", "both"]:
			m.add_radiobutton(label=s, variable=self.arrow)

		m_button.grid(column=0, row=10, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="arrowshape:")
		lbl1.grid(column=0, row=11, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		xlbl = tkinter.Label(self, text="l1: ")
		xlbl.grid(column=0, row=12)

		e1 = tkinter.Entry(self, textvariable=self.l1, width=4)
		e1.grid(column=1, row=12)

		ylbl = tkinter.Label(self, text="l2: ")
		ylbl.grid(column=2, row=12)

		e2 = tkinter.Entry(self, textvariable=self.l2, width=4)
		e2.grid(column=3, row=12)

		ylbl = tkinter.Label(self, text="l3: ")
		ylbl.grid(column=4, row=12)

		e2 = tkinter.Entry(self, textvariable=self.l3, width=4)
		e2.grid(column=5, row=12)

		lbl1 = tkinter.Label(self, text="fill:")
		lbl1.grid(column=0, row=13, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.fill, width=10)
		e5.grid(column=0, row=14, columnspan=10, sticky=tkinter.W)

		chk = tkinter.Checkbutton(self, text="smooth", variable=self.smooth)
		chk.grid(column=0, row=15, columnspan=10, sticky=tkinter.W, pady=20)

		lbl1 = tkinter.Label(self, text="state:")
		lbl1.grid(column=0, row=16, columnspan=10, sticky=tkinter.W)


		m_button = tkinter.Menubutton(self, textvariable=self.state, relief=tkinter.SOLID, borderwidth=1, height=1,
		                              width=10)
		m = tkinter.Menu(m_button)

		m_button["menu"] = m

		for s in ["normal", "disabled", "hidden"]:
			m.add_radiobutton(label=s, variable=self.state)

		m_button.grid(column=0, row=17, pady=(10, 0), columnspan=10, sticky=tkinter.W)

		lbl1 = tkinter.Label(self, text="width:")
		lbl1.grid(column=0, row=18, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.width, width=10)
		e5.grid(column=0, row=19, columnspan=10, sticky=tkinter.W)

		lbl = tkinter.Label(self, text="tags")
		lbl.grid(column=0, row=20, columnspan=10, pady=(10, 0), sticky=tkinter.W)

		e5 = tkinter.Entry(self, textvariable=self.tags, width=30)
		e5.grid(column=0, row=21, columnspan=10, sticky=tkinter.W)

		sub_btn = tkinter.Button(self, command=self.submit, textvariable=self.stxt)
		sub_btn.grid(column=0, row=22, pady=40, columnspan=10, sticky=tkinter.W)

	def draw(self, l):
		if len(l) == 2:
			return
		self.lb.delete(0, tkinter.END)
		a = list()

		for i, x in enumerate(l):
			if i % 2 == 0:
				a.append(f"({l[i]},{l[i+1]})")

		self.lb.insert(tkinter.END, *a)

		return self.submit()

	def modify(self):
		i = self.lb.curselection()

		if len(i) == 0:
			tmb.showerror("Errore", "Non opzione selezionato")
			return
		i = i[0]
		x, y = self.x1.get(), self.y1.get()
		self.lb.delete(i)
		self.lb.insert(i, f"({x}, {y})")

	def submit(self):
		res = utilities.are_number(self.width, self.l1, self.l2, self.l3)

		if not res:
			tmb.showerror("Errore", "Alcuni parametri richiedono dei numeri interi.")
			return

		if self.width.get() <= 0:
			tmb.showerror("Errore", "'width' deve essere posivito")
			return

		if self.l1.get() < 0:
			tmb.showerror("Errore", "'l1' deve essere posivito")
			return

		if self.l2.get() < 0:
			tmb.showerror("Errore", "'l2' deve essere posivito")
			return

		if self.l3.get() < 0:
			tmb.showerror("Errore", "'width' deve essere posivito")
			return

		if self.lb.size() == 0:
			return

		coos = []

		items: Tuple[str] = self.lb.get(0, tkinter.END)

		for item in items:
			item = item[1:-1]

			c = item.split(",")
			x = float(c[0])
			y = float(c[1])

			coos.append((x, y))

		if self.smooth.get() == 0:
			s = False
		else:
			s = True
		tags = [x.strip() for x in list(filter(lambda x: len(x.strip()) != 0, self.tags.get().split(",")))]

		try:
			if self.id is None:
				return self.c.create_line(*coos, width=self.width.get(), smooth=s, fill=self.fill.get(),tags=tags, state=self.state.get(), arrowshape=(self.l1.get(), self.l2.get(), self.l3.get()), arrow=self.arrow.get())
			else:
				c = [y for x in coos for y in x]
				self.c.coords(self.id, c)
				self.c.itemconfigure(self.id, width=self.width.get(), smooth=s, fill=self.fill.get(),tags=tags, state=self.state.get(), arrowshape=(self.l1.get(), self.l2.get(), self.l3.get()), arrow=self.arrow.get())
				self.master.master.master.reset([])
				self.master.master.master.showSelected()
		except Exception as e:
			tmb.showerror("Errore", str(e))
			return

	def ditem(self):
		if self.lb.size() != 0 and len(self.lb.curselection()) != 0:
			i = self.lb.curselection()[0]
			self.lb.delete(i)

	def check_coo(self):
		res = utilities.are_number(self.x1, self.y1)

		if not res:
			tmb.showerror("Errore", "Alcuni parametri richiedono dei numeri interi.")
			return False

		return True

	def add_below(self):
		if not self.check_coo():
			return

		if self.lb.size() != 0 and len(self.lb.curselection()) != 0:
			i = self.lb.curselection()[0]

			self.lb.insert(i+1, f"({self.x1.get()}, {self.y1.get()})")

		else:
			self.lb.insert(tkinter.END, f"({self.x1.get()}, {self.y1.get()})")

	def add_above(self):
		if not self.check_coo():
			return

		if self.lb.size() != 0:
			if len(self.lb.curselection()) != 0:
				i = self.lb.curselection()[0]

				self.lb.insert(i, f"({self.x1.get()}, {self.y1.get()})")

		else:
			self.lb.insert(tkinter.END, f"({self.x1.get()}, {self.y1.get()})")