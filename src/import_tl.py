import tkinter
import tkinter.filedialog as tf
import canvas_parser

class Import_tl(tkinter.Toplevel):
	def __init__(self, master, c):
		super().__init__(master=master)

		self.canvas = c
		self.geometry("400x200+800+200")
		self.opt = tkinter.IntVar(value=0)

		self.create_widget()

	def create_widget(self):
		main_frame = tkinter.Frame(self)
		main_frame.grid()

		r = tkinter.Radiobutton(self, text="Json file (.json)", value=0, variable=self.opt)
		r.grid(column=0, row=1, sticky="w")

		p = tkinter.Button(self, text="Importa", command=self.imp)
		p.grid(column=0, row=2, columnspan=5, pady=10)

	def imp(self):
		p = canvas_parser.Parser(self.canvas)
		if self.opt.get() == 0:
			f = tf.askopenfilename()
			p.filename = f
			p.from_json()

		self.destroy()
